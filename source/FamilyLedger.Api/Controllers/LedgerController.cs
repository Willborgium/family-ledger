﻿using FamilyLedger.Api.Models;
using FamilyLedger.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Twilio.AspNet.Core;
using Twilio.TwiML;
using Twilio.TwiML.Messaging;

namespace FamilyLedger.Api.Controllers
{
    [Route("api/ledger")]
    public class LedgerController : TwilioController
    {
        private readonly ILedgerService _ledgerService;

        public LedgerController(ILedgerService ledgerService)
        {
            _ledgerService = ledgerService;
        }

        [HttpGet]
        [Route("health/{message}")]
        public IActionResult Health([FromRoute] string message)
        {
            return Ok($"server heard {message}");
        }

        [HttpPost]
        [Route("twilio")]
        public IActionResult ProcessActionFromTwilio([FromForm(Name = "Body")]string body)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(body))
                {
                    return BadRequest($"heard '{body}'");
                }

                var parts = body.Split(' ');

                string responseText;

                Guid idToFind;

                switch (parts[0].ToLower())
                {
                    case "view":
                        if (Guid.TryParse(parts[1], out idToFind))
                        {
                            var action = _ledgerService.GetAction(idToFind);
                            responseText = FormatAction(action);
                        }
                        else
                        {
                            if (parts.Length > 2)
                            {
                                if (int.TryParse(parts[2], out var count))
                                {
                                    var actions = _ledgerService.ViewActions(parts[1], count);
                                    responseText = string.Join("\n", actions.Select(FormatAction));
                                }
                                else
                                {
                                    return BadRequest($"expected number, got {parts[2]}");
                                }
                            }
                            else
                            {
                                var results = _ledgerService.ViewAccount(parts[1]);
                                responseText = string.Join("\n", results);
                            }
                        }
                        break;

                    default:
                        if (Guid.TryParse(parts[0], out idToFind))
                        {
                            var action = _ledgerService.GetAction(idToFind);
                            responseText = FormatAction(action);
                        }
                        else
                        {
                            var id = _ledgerService.CreateAction(parts);
                            responseText = $"{id}";
                        }
                        break;
                };

                var message = new Message();
                message.Body(responseText);
                var response = new MessagingResponse();
                response.Append(message);

                return TwiML(response);
            }
            catch (ArgumentException exception)
            {
                return BadRequest(exception.Message);
            }
        }

        private static string FormatAction(LedgerAction action)
        {
            var output = string.Empty;

            output += $"{action.Date:M/d/yy}: {action.From} {action.Type} {action.To} {action.Amount:C}";

            if (!string.IsNullOrWhiteSpace(action.Description))
            {
                output += $" for {action.Description}";
            }

            return output;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult ViewLedger()
        {
            return Ok(_ledgerService.Data);
        }

        [HttpGet]
        [Route("delete/{id}")]
        public IActionResult DeleteAction([FromRoute]Guid id)
        {
            if (_ledgerService.DeleteAction(id))
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("delete")]
        public IActionResult DeleteAll()
        {
            _ledgerService.DeleteActions();
            
            return Ok();
        }
    }
}
