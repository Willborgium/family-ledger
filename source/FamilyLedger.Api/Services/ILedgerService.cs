﻿using FamilyLedger.Api.Models;

namespace FamilyLedger.Api.Services
{
    public interface ILedgerService
    {
        LedgerData Data { get; }

        Guid CreateAction(string[] parts);

        LedgerAction GetAction(Guid id);

        IEnumerable<string> ViewAccount(string name);

        IEnumerable<LedgerAction> ViewActions(string account, int count);

        bool DeleteAction(Guid id);

        void DeleteActions();
    }
}
