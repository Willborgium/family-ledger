﻿using FamilyLedger.Api.Models;
using Newtonsoft.Json;
using System.Globalization;

namespace FamilyLedger.Api.Services
{
    public class LedgerService : ILedgerService
    {
        public LedgerData Data => _data;

        public LedgerService()
        {
            string fileData;

            using (var reader = new StreamReader(_ledgerFilePath))
            {
                fileData = reader.ReadToEnd();
            }

            _data = JsonConvert.DeserializeObject<LedgerData>(fileData);

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        }

        public Guid CreateAction(string[] parts)
        {
            if (parts.Length < 4)
            {
                throw new ArgumentException($"server expected at least 4 parts but received {parts.Length}");
            }

            var from = parts[0].ToLower();
            var type = parts[1].ToLower() switch
            {
                "owes" or "owe" or "owed" => LedgerActionType.Owes,
                "paid" or "pay" or "pays" => LedgerActionType.Paid,
                _ => throw new ArgumentException($"invalid operation. cannot parse '{parts[1]}' into an action type"),
            };

            var to = parts[2].ToLower();

            if (!decimal.TryParse(parts[3], NumberStyles.Any, CultureInfo.CurrentCulture.NumberFormat, out var amount))
            {
                throw new ArgumentException($"invalid amount. cannot parse '{parts[3]}'");
            }

            string? description = null;

            if (parts.Length > 4)
            {
                var startIndex = 4;

                if (parts[4].ToLower() == "for" && parts.Length > 5)
                {
                    startIndex++;
                }

                description = string.Join(" ", parts.Skip(startIndex));
            }

            var action = new LedgerAction
            {
                From = from,
                To = to,
                Amount = (float)amount,
                Type = type,
                Description = description
            };

            _data.Actions.Add(action);

            SaveData();

            return action.Id;
        }

        public LedgerAction GetAction(Guid id)
        {
            return _data.Actions.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<string> ViewAccount(string name)
        {
            var lName = name.ToLower();

            var owedAmounts = new Dictionary<string, float>();

            void AddOrUpdate(string name, float amount)
            {
                if (owedAmounts.ContainsKey(name))
                {
                    owedAmounts[name] += amount;
                }
                else
                {
                    owedAmounts.Add(name, amount);
                }
            }

            foreach (var action in _data.Actions)
            {
                if (action.From == lName)
                {
                    switch (action.Type)
                    {
                        case LedgerActionType.Paid:
                            AddOrUpdate(action.To, -action.Amount);
                            break;

                        case LedgerActionType.Owes:
                            AddOrUpdate(action.To, action.Amount);
                            break;
                    }
                }
                else if (action.To == lName)
                {
                    switch (action.Type)
                    {
                        case LedgerActionType.Paid:
                            AddOrUpdate(action.From, action.Amount);
                            break;

                        case LedgerActionType.Owes:
                            AddOrUpdate(action.From, -action.Amount);
                            break;
                    }
                }
            }

            var output = new List<string>();

            foreach ((var person, var amount) in owedAmounts)
            {
                if (amount > 0)
                {
                    output.Add($"{lName} owes {person} {amount:C}");
                }
                else if (amount < 0)
                {
                    var x = Math.Abs(amount);
                    output.Add($"{person} owes {lName} {x:C}");
                }
            }

            if (!output.Any())
            {
                return AllDebtsArePaid;
            }

            return output;
        }

        public IEnumerable<LedgerAction> ViewActions(string account, int count)
        {
            var output = new List<LedgerAction>();

            foreach (var action in _data.Actions.OrderByDescending(a => a.Date))
            {
                if (action.From == account || action.To == account)
                {
                    output.Add(action);
                }

                if (output.Count == count)
                {
                    break;
                }
            }

            return output;
        }

        public bool DeleteAction(Guid id)
        {
            var action = _data.Actions.FirstOrDefault(a => a.Id == id);

            if (action == null)
            {
                return false;
            }

            _data.Actions.Remove(action);

            SaveData();

            return true;
        }

        public void DeleteActions()
        {
            _data.Actions.Clear();

            SaveData();
        }

        private static readonly string[] AllDebtsArePaid = new[] { "All debts are paid" };

        private void SaveData()
        {
            var jsonData = JsonConvert.SerializeObject(_data);

            using (var writer = new StreamWriter(_ledgerFilePath))
            {
                writer.Write(jsonData);
            }
        }

        private const string _ledgerFilePath = "ledger.json";
        private readonly LedgerData _data;
    }
}
