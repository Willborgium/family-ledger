using FamilyLedger.Api.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddScoped<ILedgerService, LedgerService>();

var app = builder.Build();

app.MapControllers();

app.Run();