﻿namespace FamilyLedger.Api.Models
{
    public class LedgerAction
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime Date { get; set; } = DateTime.Now;
        public string From { get; set; }
        public string To { get; set; }
        public float Amount { get; set; }
        public LedgerActionType Type { get; set; }
        public string? Description { get; set; }
    }
}