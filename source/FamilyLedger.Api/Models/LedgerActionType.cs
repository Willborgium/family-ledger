﻿namespace FamilyLedger.Api.Models
{
    public enum LedgerActionType
    {
        Owes,
        Paid
    }
}