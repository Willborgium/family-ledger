﻿namespace FamilyLedger.Api.Models
{
    public class LedgerData
    {
        public List<LedgerAction> Actions { get; set; }
    }
}